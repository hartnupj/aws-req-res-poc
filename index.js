const R = require('ramda');
const AWS = require('aws-sdk');
const express = require('express');
const NodeCache = require('node-cache');
const uuidV4 = require('uuid/v4');

const gatewayId = uuidV4();
const TOPIC_ARN = 'arn:aws:sns:eu-west-1:963507507145:reqres';
const STATE_MACHINE_ARN = 'arn:aws:states:eu-west-1:963507507145:stateMachine:ComparisonStepFunctionsStateMachine-IaxS89iG1XxB';

AWS.config.update({
  region: 'eu-west-1'
});

const SQS = new AWS.SQS({ apiVersion: '2012-11-05' });
const SNS = new AWS.SNS({ apiVersion: '2010-03-31' });
const stepfunctions = new AWS.StepFunctions({apiVersion: '2016-11-23'});

// Create a queue


// Subscribe
const subscribe = async (sqsArn) => {
  const params = {
    Protocol: 'sqs',
    TopicArn: TOPIC_ARN,
    Attributes: {
      FilterPolicy: JSON.stringify({
          gatewayId: [gatewayId]
        }
      )
    },
    Endpoint: sqsArn,
  };

  const response = await SNS.subscribe(params)
    .promise();
  console.log({ response });
  return response;

};

const createQueue = async () => {
  const params = {
    QueueName: `reqres-${gatewayId}`,
  };
  const { QueueUrl } = await SQS.createQueue(params)
    .promise();
  const attributes = await SQS.getQueueAttributes(
    {
      QueueUrl,
      AttributeNames: ['QueueArn'],
    })
    .promise();
  console.log({ attributes });
  const setPolicyResult = await SQS.setQueueAttributes({
    QueueUrl,
    Attributes: {
      Policy: JSON.stringify({
          'Id': 'MyQueuePolicy',
          'Statement': [{
            'Resource': attributes.Attributes.QueueArn,
            'Effect': 'Allow',
            'Principal': '*',
            'Action': ['SQS:SendMessage','SQS:DeleteMessage'],
            'Condition': {
              'ArnEquals': {
                'aws:SourceArn': TOPIC_ARN,
              }
            }
          }]
        }
      ),
    }
  }).promise();
  console.log({setPolicyResult});
  return { ...attributes.Attributes, QueueUrl };
};

const forward = async (body) => {
  try {
    const params = {
      stateMachineArn: STATE_MACHINE_ARN,
      input: JSON.stringify(body),
    };
    const result = await stepfunctions.startExecution(params).promise();
    console.log('Submitted execution', result);
    return result;
  } catch (e) {
    console.error('StepFunction error', e);
  }
};

const pendingResponses = new NodeCache({
  stdTTL: 5,
  checkperiod: 1,
  useClones: false,
});

pendingResponses.on('expired', (key, res) => {
  console.log(`timeout on ${key}`);
  res.status(504);
  res.send('<h1>504 Timeout</h1><p>The request timed out</p>');
  res.end();
});

const frontendApp = express();
const frontendPort = 8080;

frontendApp.use(express.json());

const sendToCorrelatedExpressResponse = data => {
  const body = JSON.parse(data.Body);
  const correlationId = body.MessageAttributes.correlationId.Value;
  const message = body.Message;
  pendingResponses.get(correlationId, (err, res) => {
    if (err) {
      console.error(err);
    } else if (res) {
      pendingResponses.del(correlationId);
      res.json(JSON.parse(message));
      res.end();
    } else {
      console.error('Received response for expired request');
    }
  });
};

frontendApp.post('/', async (req, res) => {
  const { body } = req;
  const uuid = uuidV4();
  pendingResponses.set(uuid, res);

  try {
    await forward({
        gatewayId,
        correlationId: uuid,
         ...body,
      },
    );
  } catch (err) {
    console.log(`Failed: will not retry: ${err}`);
  }
});


frontendApp.get('/', async (req, res) => {
  res.json({ hello: 'world' });
  res.end();
});


frontendApp.listen(frontendPort, () => console.log(`Frontend listening on port ${frontendPort}`));

const deleteMessage = async (queueUrl, data) => {
  const params = {
    QueueUrl: queueUrl,
    ReceiptHandle: data.Messages[0].ReceiptHandle,
  };
  const result = await SQS.deleteMessage(params)
    .promise();
  return result;
};

async function setup() {
  const { QueueArn, QueueUrl } = await createQueue();
  const sub = await (subscribe(QueueArn));
  console.log(sub);
  return ({ QueueArn, QueueUrl });
}

async function pollForMessage({ QueueUrl }) {
  console.log('Polling');
  try {
    const params = {
      AttributeNames: [
        'SentTimestamp',
        'gatewayId',
      ],
      MaxNumberOfMessages: 10,
      MessageAttributeNames: [
        'All'
      ],
      QueueUrl: QueueUrl,
      VisibilityTimeout: 20,
      WaitTimeSeconds: 20,
    };
    const data = await SQS.receiveMessage(params)
      .promise();
    console.log('Got data!', data);
    if (data.Messages) {
      await deleteMessage(QueueUrl, data);
      data.Messages.forEach(message => {
          sendToCorrelatedExpressResponse(message);
        }
      );
    }
  } catch (err) {
    console.log('Queue err', err);
  } finally {
    pollForMessage({ QueueUrl }); // I'm not sure whether this being an async function means it's not growing a stack.
  }
}

setup()
  .then(data => pollForMessage(data));
