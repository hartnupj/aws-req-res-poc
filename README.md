# Request/Response over AWS Proof of concept

What's this?

Say you've got an asynchronous operation that lives on the cloud. Perhaps AWS
Step functions. They can do something when the task is complete (like send a 
notification), but they have no link back to the process that initiated the
operation.

Then, say you've got a client that's initiated the operation, and would like to
receive the result in a synchronous response; the traditional HTTP, REST, etc. way.

This POC completes that circle: 

 - an HTTP server acts as a gateway for the client
 - the server attaches a 'gateway ID' and a 'correlation ID' to each request it
   submits to the async cloud.
 - at the end of the async process, a step is expected to publish a message to
   an SNS topic, containing the node ID, correlation ID, and a result
 - The gateway server subscribes to the topic using SQS, so is notified 
   immediately, and can put the result into a response to the still-alive
   HTTP request.
   
Since each gateway filters its subscription by gateway ID, this system *should* scale
horizontally.


## To run it

Run it on your laptop:

 - [Authenticate to AWS in your CLI](https://wealthwizards.atlassian.net/wiki/spaces/ENG/pages/1413940058/AWS+login+using+Office365+credentials+-+cli+keys+and+console+access)
 - `AWS_PROFILE=proof_access yarn start`

If you get errors about the SNS topic not existing, create a new one and edit `TOPIC_ARN` in
index.js. 


## TODO:

 - ~~Need to call a real async service on the network. Probably a Step function
   workflow. Currently (just because I ran out of time) we have a JS async
   function in place of this.~~
 - Tidy up the code: modularise, unit test, ...
 - Configurable HTTP route -> step function mappings
 - Make it deploy in Kubernetes
 - Consider results larger than an SNS/SQS message can contain: perhaps the
   SNS message can contain a reference to an S3 resource.
 - Consider security: 
     - is it necessary to have digital signatures on any of the
       messages?
     - how about carrying a JWT through the async cloud and
       authenticating/authorising each step?
 - Do some performance/load testing
    - quantify capacity in EC2 instance types
    - demonstrate horizontal scaling
    - quantify AWS costs per request
 - Add timestamp and time-to-live to the attributes sent downstream
   so that the async processes in the cloud can abandon
   requests too old to be useful
 - Each instance creates one queue, which is left behind when
   the instance terminates. Find a way to clean these up. Orderly
   shutdown can include deleting a queue, but not all shutdowns are
   orderly.
    - (Last ditch) idea: gateway nodes write a heartbeat to
     consul/whatever. Scheduled job deletes any queues
     not associated with an active heartbeat.
    - (Even laster ditch) idea: Gateway nodes always terminate
     after a set period (for Kube/whatever to replace). A scheduled
     job deletes queues older than that.
